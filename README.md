# MOVIES #

Simple app connecting to the https://www.themoviedb.org/ API and shows the popular movies

### What is this repository for? ###

* To deliver the sample app for my interview in Usertech

### How do I get set up? ###

* Open project in Android Studio
* or use the Gradle wrapper to generate the APK and install it using ADB