package com.pavelskala.movies.db

import com.pavelskala.movies.model.Movie
import com.pavelskala.movies.util.extensions.parseList
import com.pavelskala.movies.util.extensions.parseSingle
import org.jetbrains.anko.db.*

/**
 * Manipulates data in DB using given dbHelper
 *
 * Created by Pavel on 31.1.2017.
 */
class DefaultMoviesDb(private val dbHelper: ManagedSQLiteOpenHelper) : MoviesDb {

    override fun getPopularMovies(): List<Movie> = dbHelper.use {
        select(PopularMoviesTable.TABLE_NAME).parseList(mapRowParser())
    }

    override fun addPopularMovies(movies: List<Movie>) = dbHelper.use {
        transaction {
            PopularMoviesTable.apply {
                movies.forEach {
                    insert(TABLE_NAME,
                        ID to it.id,
                        TITLE to it.title,
                        OVERVIEW to it.overview,
                        POSTER to it.poster,
                        BACKDROP to it.backdrop,
                        VOTE_AVERAGE to it.popularity,
                        VOTE_COUNT to it.votes,
                        RELEASE_DATE to it.releaseDate)
                }
            }
        }
    }

    override fun replacePopularMovies(movies: List<Movie>) = dbHelper.use {
        transaction {
            PopularMoviesTable.apply {
                delete(TABLE_NAME)
                movies.forEach {
                    insert(TABLE_NAME,
                        ID to it.id,
                        TITLE to it.title,
                        OVERVIEW to it.overview,
                        POSTER to it.poster,
                        BACKDROP to it.backdrop,
                        VOTE_AVERAGE to it.popularity,
                        VOTE_COUNT to it.votes,
                        RELEASE_DATE to it.releaseDate)
                }
            }
        }
    }

    override fun getPopularMovie(id: Long): Movie  = dbHelper.use {
        select(PopularMoviesTable.TABLE_NAME)
                .where("${PopularMoviesTable.ID} = $id")
                .parseSingle(mapRowParser())
    }

    /**
     * Parses Map representing one row of PopularMoviesTable to the domain model object
     */
    private fun mapRowParser(): (Map<String, Any?>) -> Movie {
        return {
            Movie(
                    it[PopularMoviesTable.ID] as Long? ?: 0,
                    it[PopularMoviesTable.TITLE] as String? ?: "",
                    it[PopularMoviesTable.OVERVIEW] as String? ?: "",
                    it[PopularMoviesTable.POSTER] as String? ?: "",
                    it[PopularMoviesTable.BACKDROP] as String? ?: "",
                    it[PopularMoviesTable.VOTE_AVERAGE] as Double? ?: 0.0,
                    it[PopularMoviesTable.VOTE_COUNT] as Long? ?: 0,
                    it[PopularMoviesTable.RELEASE_DATE] as String? ?: ""
            )
        }
    }
}