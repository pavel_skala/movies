package com.pavelskala.movies.db

import com.pavelskala.movies.model.Movie

/**
 * Manipulates Movies data in the DB
 *
 * Created by Pavel on 31.1.2017.
 */
interface MoviesDb {

    fun getPopularMovies(): List<Movie>
    fun addPopularMovies(movies: List<Movie>)
    fun replacePopularMovies(movies: List<Movie>)
    fun getPopularMovie(id: Long): Movie

}