package com.pavelskala.movies.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

/**
 * BDHelper to be used for data manipulation
 *
 * Created by Pavel on 31.1.2017.
 */
class MoviesDBHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, DB_NAME, null, DB_VERSION) {

    companion object {
        /**
         * Name of the file to store the db into
         */
        const val DB_NAME = "movies.db"

        /**
         * Current version of the DB to determine when to upgrade
         */
        const val DB_VERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase) {
        PopularMoviesTable.apply {
            db.createTable(TABLE_NAME, true,
                    _ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                    ID to INTEGER,
                    TITLE to TEXT,
                    OVERVIEW to TEXT,
                    POSTER to TEXT,
                    BACKDROP to TEXT,
                    VOTE_AVERAGE to REAL,
                    VOTE_COUNT to INTEGER,
                    RELEASE_DATE to TEXT
                )
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(PopularMoviesTable.TABLE_NAME, true)
        onCreate(db)
    }
}