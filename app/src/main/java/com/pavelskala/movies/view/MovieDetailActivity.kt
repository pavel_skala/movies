package com.pavelskala.movies.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.pavelskala.movies.MoviesApplication
import com.pavelskala.movies.MoviesConfig
import com.pavelskala.movies.MoviesConfig.EXTRA_MOVIE_ID
import com.pavelskala.movies.R
import com.pavelskala.movies.databinding.ActivityMovieDetailBinding
import com.pavelskala.movies.model.DataProvider
import com.pavelskala.movies.viewmodel.MovieDetailActivityViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.android.synthetic.main.item_movie.view.*
import org.jetbrains.anko.toast
import javax.inject.Inject

/**
 * Activity showing the detail of a movie
 */
class MovieDetailActivity : AppCompatActivity(), MovieDetailActivityViewModel.DataListener {

    @Inject lateinit var dataProvider: DataProvider
    lateinit var viewModel: MovieDetailActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoviesApplication.dataComponent.inject(this)
        val binding: ActivityMovieDetailBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_movie_detail)
        setSupportActionBar(toolbar_detail)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val fragment = supportFragmentManager.findFragmentByTag(ViewModelRetainingFragment.TAG)
        if (fragment == null) {
            viewModel = MovieDetailActivityViewModel(intent.getLongExtra(EXTRA_MOVIE_ID, 0L), dataProvider)
            supportFragmentManager.beginTransaction().add(ViewModelRetainingFragment(viewModel),
                    ViewModelRetainingFragment.TAG).commit()
        } else {
            viewModel = (fragment as ViewModelRetainingFragment).viewModel
        }
        binding.viewModel = viewModel
        viewModel.attach(this)
        viewModel.loadData()
    }

    override fun onDestroy() {
        viewModel.destroy()
        super.onDestroy()
    }

    override fun showPicture(filename: String) {
        Picasso
            .with(this)
            .load("${MoviesConfig.IMAGES_BASE_URL}${MoviesConfig.IMAGE_SIZE_LARGE}/$filename")
            .into(image_detail_movie)
    }

    override fun showError(e: Throwable) {
        toast(R.string.error_detail_loading)
    }

    /**
     * Fragment for storing the viewmodel during handling the configuration changes
     */
    class ViewModelRetainingFragment(val viewModel: MovieDetailActivityViewModel) : Fragment() {
        companion object {
            const val TAG = "ViewModelRetainingFragment"
        }
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            retainInstance = true
        }
    }
}
