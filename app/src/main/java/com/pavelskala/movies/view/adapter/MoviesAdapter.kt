package com.pavelskala.movies.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavelskala.movies.MoviesConfig
import com.pavelskala.movies.R
import com.pavelskala.movies.databinding.ItemMovieBinding
import com.pavelskala.movies.model.Movie
import com.pavelskala.movies.viewmodel.MovieItemViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*

/**
 * Adapter showing the list of the movies
 *
 * Created by Pavel on 1.2.2017.
 */
class MoviesAdapter : RecyclerView.Adapter<MoviesAdapter.MoviesListViewHolder>() {

    companion object {
        const val TYPE_ITEM = 0
        const val TYPE_LOADING = 1
    }

    private val data: MutableList<Movie> = mutableListOf()

    override fun getItemCount(): Int = data.size + 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesListViewHolder =
        when (viewType) {
            TYPE_ITEM -> {
                    val binding: ItemMovieBinding = DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context),
                            R.layout.item_movie,
                            parent,
                            false
                        )
                    val viewModel = MovieItemViewModel(parent.context)
                    binding.viewModel = viewModel
                    MovieViewHolder(binding)
                }
            else -> LoadingViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.item_movie_loading, parent, false))
        }

    override fun onBindViewHolder(holder: MoviesListViewHolder, position: Int) {
        if (position < data.size) {
            val movieViewHolder = holder as MovieViewHolder
            val movie = data[position]
            movieViewHolder.bind(movie)
            Picasso
                .with(movieViewHolder.itemView.context)
                .load("${MoviesConfig.IMAGES_BASE_URL}${MoviesConfig.IMAGE_SIZE_SMALL}/${movie.poster}")
                .noFade() // because of the CircleImageView
                .into(movieViewHolder.itemView.image_item_movie)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position < data.size) {
            return TYPE_ITEM
        } else {
            return TYPE_LOADING
        }
    }

    /**
     * Replaces all the data of the adapter and refreshes the whole adapter
     */
    fun setData(data: List<Movie>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    /**
     * Adds the given data to the current data of the adapter
     */
    fun addData(data: List<Movie>) {
        val position = this.data.size
        this.data.addAll(data)
        notifyItemRangeInserted(position, data.size)
    }

    abstract class MoviesListViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class MovieViewHolder(private val binding: ItemMovieBinding) :
            MoviesListViewHolder(binding.layoutItemMovie) {

        fun bind(movie: Movie) {
            binding.viewModel.setData(movie)
        }

    }

    class LoadingViewHolder(view: View) : MoviesListViewHolder(view)

}