package com.pavelskala.movies.view

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavelskala.movies.MoviesApplication
import com.pavelskala.movies.R
import com.pavelskala.movies.model.DataProvider
import com.pavelskala.movies.model.Movie
import com.pavelskala.movies.view.adapter.MoviesAdapter
import com.pavelskala.movies.viewmodel.MainFragmentViewModel
import javax.inject.Inject
import kotlinx.android.synthetic.main.fragment_main.view.*
import org.jetbrains.anko.toast

/**
 * Fragment showing the list of movies
 *
 * Created by Pavel on 31.1.2017.
 */
class MainFragment : Fragment(), MainFragmentViewModel.DataListener {

    private val moviesAdapter: MoviesAdapter = MoviesAdapter()
    private val visibleThreshold = 3

    @Inject lateinit var dataProvider: DataProvider
    private lateinit var refresher: SwipeRefreshLayout
    private lateinit var connectivityManager: ConnectivityManager
    private var viewModel: MainFragmentViewModel? = null
    private var isLoadingNextPage = false
    private var isRemoteLoadPossible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
              savedInstanceState: Bundle?): View? {
        MoviesApplication.dataComponent.inject(this)
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        if (viewModel == null) {
            viewModel = MainFragmentViewModel(dataProvider)
        }
        val recycler = view.recycler_movies
        val linearLayoutManager = LinearLayoutManager(activity,
                LinearLayoutManager.VERTICAL, false)
        recycler.setHasFixedSize(true)
        recycler.layoutManager = linearLayoutManager
        recycler.adapter = moviesAdapter
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = linearLayoutManager.itemCount
                val lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoadingNextPage && isRemoteLoadPossible && lastVisibleItem > 0
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    isLoadingNextPage = viewModel!!.loadNextPage()
                }
            }
        })
        view.refresh_movies.setOnRefreshListener { if (isRemoteLoadPossible) viewModel!!.loadData(true) }
        refresher = view.refresh_movies
        return view
    }

    override fun onStart() {
        viewModel!!.attach(this)
        viewModel!!.loadData(false)
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        val activeNetworkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        isRemoteLoadPossible = activeNetworkInfo?.isConnectedOrConnecting ?: false
    }

    override fun onStop() {
        viewModel!!.detach()
        super.onStop()
    }

    override fun onDestroy() {
        viewModel!!.destroy()
        super.onDestroy()
    }

    override fun showLoading(loading: Boolean) {
        refresher.isRefreshing = loading
    }

    override fun showData(data: List<Movie>) {
        moviesAdapter.setData(data)
        isLoadingNextPage = false
    }

    override fun addData(data: List<Movie>) {
        moviesAdapter.addData(data)
        isLoadingNextPage = false
    }

    override fun showLoadingError(e: Throwable) {
        activity.toast(R.string.error_movies_loading)
    }

}