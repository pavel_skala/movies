package com.pavelskala.movies.di

import android.content.Context
import com.pavelskala.movies.db.MoviesDb
import com.pavelskala.movies.model.DataProvider
import com.pavelskala.movies.model.DefaultDataProvider
import com.pavelskala.movies.model.MoviesClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * DI module providing DataProvider
 *
 * Created by Pavel on 31.1.2017.
 */
@Module
class DataModule {

    @Provides
    @Singleton
    fun provideDataProvider(ctx: Context, db: MoviesDb, moviesClient: MoviesClient): DataProvider
            = DefaultDataProvider(ctx, db, moviesClient)

}