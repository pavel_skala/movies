package com.pavelskala.movies.di

import android.content.Context
import com.pavelskala.movies.db.DefaultMoviesDb
import com.pavelskala.movies.db.MoviesDBHelper
import com.pavelskala.movies.db.MoviesDb
import dagger.Module
import dagger.Provides
import org.jetbrains.anko.db.ManagedSQLiteOpenHelper
import javax.inject.Singleton

/**
 * DI module providing database features
 *
 * Created by Pavel on 31.1.2017.
 */
@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideMoviesDb(sqLiteOpenHelper: ManagedSQLiteOpenHelper): MoviesDb = DefaultMoviesDb(sqLiteOpenHelper)

    @Provides
    @Singleton
    fun provideSQLiteOpenHelper(ctx: Context): ManagedSQLiteOpenHelper = MoviesDBHelper(ctx)

}