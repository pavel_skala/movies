package com.pavelskala.movies.di

import com.pavelskala.movies.view.MainFragment
import com.pavelskala.movies.view.MovieDetailActivity
import dagger.Component
import javax.inject.Singleton

/**
 * The main DI component of the project
 *
 * Created by Pavel on 31.1.2017.
 */
@Singleton
@Component(modules = arrayOf(
        ApplicationModule::class,
        DatabaseModule::class,
        NetworkModule::class,
        DataModule::class
))
interface DataComponent {

    fun inject(fragment: MainFragment)
    fun inject(activity: MovieDetailActivity)

}