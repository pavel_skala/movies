package com.pavelskala.movies.di

import android.content.Context
import com.pavelskala.movies.MoviesApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * DI module providing Application and Context
 *
 * Created by Pavel on 31.1.2017.
 */
@Module
class ApplicationModule(private val app: MoviesApplication) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideApplication(): MoviesApplication = app

}