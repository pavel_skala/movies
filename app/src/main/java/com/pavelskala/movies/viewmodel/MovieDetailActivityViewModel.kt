package com.pavelskala.movies.viewmodel

import android.databinding.ObservableDouble
import android.databinding.ObservableField
import android.databinding.ObservableLong
import android.util.Log
import com.pavelskala.movies.model.DataProvider
import com.pavelskala.movies.model.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ViewModel for the activity showing the detail of movie
 *
 * Created by Pavel on 1.2.2017.
 */
class MovieDetailActivityViewModel(
        private val movieId: Long,
        private val dataProvider: DataProvider) {

    companion object {
        private const val TAG = "DetailActViewModel"
    }

    val name = ObservableField<String>("")
    val releaseYear = ObservableField<String>("")
    val rating = ObservableDouble(0.0)
    val votes = ObservableLong(0L)
    val description = ObservableField<String>("")

    private var loadedMovie: Movie? = null
    private var disposable: Disposable? = null
    private var dataListener: DataListener? = null

    /**
     * Loads the movie detail from data provider or uses the cached data.
     * Triggers loading of the picture when data loaded
     */
    fun loadData() {
        val loadedMovieImmutable = loadedMovie
        if (loadedMovieImmutable == null) {
            disposable = dataProvider.getMovie(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    disposable = null
                    loadedMovie = result
                    setData(result)
                    dataListener?.showPicture(result.backdrop)
                },
                {e ->
                    disposable = null
                    Log.e(TAG, "Error when loading movie detail", e)
                    dataListener?.showError(e)
                })
        } else {
            dataListener?.showPicture(loadedMovieImmutable.backdrop)
        }
    }

    fun attach(dataListener: DataListener) {
        this.dataListener = dataListener
    }

    fun destroy() {
        this.dataListener = null
        disposable?.dispose()
    }

    fun setData(movie: Movie) {
        name.set(movie.title)
        releaseYear.set(movie.releaseDate.substring(0, 4))
        rating.set(movie.popularity)
        votes.set(movie.votes)
        description.set(movie.overview)
    }

    interface DataListener {
        fun showPicture(filename: String)
        fun showError(e: Throwable)
    }

}