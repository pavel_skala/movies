package com.pavelskala.movies.viewmodel

import android.util.Log
import com.pavelskala.movies.model.DataProvider
import com.pavelskala.movies.model.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * ViewModel for the list of the movies
 *
 * Created by Pavel on 31.1.2017.
 */
class MainFragmentViewModel(private val dataProvider: DataProvider) {

    companion object {
        private const val TAG = "MainFragmentViewModel"
    }

    private var loadDataDisposable: Disposable? = null
    private var loadNextPageDisposable: Disposable? = null
    private var dataListener: DataListener? = null
    private var loadedData: MutableList<Movie>? = null

    /**
     * Loads the initial list of data from the dataProvider or uses the cached data.
     * Sets the data to the data listener if attached
     */
    fun loadData(force: Boolean) {
        val loadedDataImmutable = loadedData
        if (loadedDataImmutable == null || force) {
            dataListener?.showLoading(true)
            loadDataDisposable = dataProvider.getData(force)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({result ->
                    loadDataDisposable = null
                    loadedData = result.toMutableList()
                    dataListener?.showLoading(false)
                    dataListener?.showData(result)
                },
                {e ->
                    loadDataDisposable = null
                    Log.e(TAG, "Error when loading movies", e)
                    dataListener?.showLoading(false)
                    dataListener?.showLoadingError(e)
                })
        } else {
            dataListener?.showData(loadedDataImmutable)
            dataListener?.showLoading(false)
        }
    }

    /**
     * Loads the next page of data (if not currently loading)
     * from the dataProvider and adds the data to the data listener if attached
     */
    fun loadNextPage(): Boolean {
        if (loadNextPageDisposable == null) {
            loadNextPageDisposable = dataProvider.getNextPage()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({result ->
                    loadNextPageDisposable = null
                    dataListener?.addData(result)
                },
                {e ->
                    loadNextPageDisposable = null
                    Log.e(TAG, "Error when loading next page of movies", e)
                    dataListener?.showLoadingError(e)
                    dataListener?.addData(listOf())
                })
            return true
        }
        return false
    }

    fun attach(dataListener: DataListener) {
        this.dataListener = dataListener
    }

    fun detach() {
        this.dataListener = null
    }

    fun destroy() {
        loadDataDisposable?.dispose()
        loadNextPageDisposable?.dispose()
    }

    interface DataListener {
        /**
         * Shows / hides the loading indicator when loading initial list of data
         */
        fun showLoading(loading: Boolean)

        /**
         * Shows the initial list of data
         */
        fun showData(data: List<Movie>)

        /**
         * Adds the next page of data
         */
        fun addData(data: List<Movie>)

        /**
         * Show error when loading data (initial or next page)
         */
        fun showLoadingError(e: Throwable)
    }
}