package com.pavelskala.movies.viewmodel

import android.content.Context
import android.content.Intent
import android.databinding.ObservableDouble
import android.databinding.ObservableField
import com.pavelskala.movies.MoviesConfig
import com.pavelskala.movies.model.Movie
import com.pavelskala.movies.view.MovieDetailActivity

/**
 * ViewModel for the item of the list of movies
 *
 * Created by Pavel on 1.2.2017.
 */
class MovieItemViewModel(private val ctx: Context) {

    val name = ObservableField<String>("")
    val score = ObservableDouble(0.0)
    private var movieId: Long? = null

    fun onItemClicked() {
        val idImmutable = movieId
        if (idImmutable != null) {
            val detailIntent = Intent(ctx, MovieDetailActivity::class.java)
            detailIntent.putExtra(MoviesConfig.EXTRA_MOVIE_ID, idImmutable)
            ctx.startActivity(detailIntent)
        }
    }

    fun setData(movie: Movie) {
        name.set(movie.title)
        score.set(movie.popularity)
        movieId = movie.id
    }
}