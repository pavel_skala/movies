package com.pavelskala.movies

/**
 * Configuration of the project properties
 *
 * Created by Pavel on 31.1.2017.
 */
object MoviesConfig {

    /**
     * The key for the API communication
     */
    const val API_KEY = BuildConfig.API_KEY

    /**
     * The base URL for the API communication
     */
    const val API_BASE_URL = "https://api.themoviedb.org/3/"

    /**
     * The base URL for downloading the images
     */
    const val IMAGES_BASE_URL = "https://image.tmdb.org/t/p/"

    /**
     * Configuration param for downloading small size images
     */
    const val IMAGE_SIZE_SMALL = "w92"

    /**
     * Configuration param for downloading large size images
     */
    const val IMAGE_SIZE_LARGE = "w780"


    /**
     * Extra, added to Intent when opening movie detail
     */
    const val EXTRA_MOVIE_ID = "com.pavelskala.movies.extra_movie_id"


    /**
     * Shared preference key to store the time the data was most recently updated from server
     */
    const val PREFERENCE_LAST_UPDATED = "pref_last_updated"

    /**
     * Shared preference key to store the last downloaded page number
     */
    const val PREFERENCE_LAST_PAGE = "pref_last_page"

}