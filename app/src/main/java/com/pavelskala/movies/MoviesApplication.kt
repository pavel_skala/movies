package com.pavelskala.movies

import android.app.Application
import com.pavelskala.movies.di.ApplicationModule
import com.pavelskala.movies.di.DaggerDataComponent
import com.pavelskala.movies.di.DataComponent
import com.pavelskala.movies.util.delegates.DelegatesExt

/**
 * The custom application class. Initializes the DI on create of the Application
 *
 * Created by Pavel on 31.1.2017.
 */
class MoviesApplication : Application() {

    companion object {
        var instance: MoviesApplication by DelegatesExt.notNullSingleValue()
        var dataComponent: DataComponent by DelegatesExt.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        dataComponent = DaggerDataComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }
}