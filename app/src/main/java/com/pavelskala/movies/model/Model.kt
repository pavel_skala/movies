package com.pavelskala.movies.model

import com.google.gson.annotations.SerializedName

/**
 * Domain class representing single movie
 *
 * Created by Pavel on 31.1.2017.
 */
class Movie() {
    @field:SerializedName("id") var id: Long = 0
    @field:SerializedName("title") var title: String = ""
    @field:SerializedName("overview") var overview: String = ""
    @field:SerializedName("poster_path") var poster: String = ""
    @field:SerializedName("backdrop_path") var backdrop: String = ""
    @field:SerializedName("vote_average") var popularity: Double = 0.0
    @field:SerializedName("vote_count") var votes: Long = 0
    @field:SerializedName("release_date") var releaseDate: String = ""

    constructor(id: Long, title: String, overview: String, poster: String, backdrop: String,
                popularity: Double, votes: Long, releaseDate: String) : this() {
        this.id = id
        this.title = title
        this.overview = overview
        this.poster = poster
        this.backdrop = backdrop
        this.popularity = popularity
        this.votes = votes
        this.releaseDate = releaseDate
    }
}

/**
 * Domain class representing downloaded list of movies
 */
class Movies() {
    @field:SerializedName("page") var page: Int = 0
    @field:SerializedName("total_pages") var totalPages: Int = 0
    @field:SerializedName("results") var movies: List<Movie> = listOf()

    constructor(page: Int, totalPages: Int, movies: List<Movie>) : this(){
        this.page = page
        this.totalPages = totalPages
        this.movies = movies
    }
}