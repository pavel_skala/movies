package com.pavelskala.movies.model

import com.pavelskala.movies.MoviesConfig
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit client for retrieval of data from the server
 *
 * Created by Pavel on 31.1.2017.
 */
interface MoviesClient {

    @GET("movie/popular")
    fun getPopularMovies(
            @Query("page") page: Int = 1,
            @Query("api_key") apiKey: String = MoviesConfig.API_KEY)
            : Single<Movies>

}