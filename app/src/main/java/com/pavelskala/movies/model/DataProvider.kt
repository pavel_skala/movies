package com.pavelskala.movies.model

import io.reactivex.Single

/**
 * Provides Observables for retrieval of the data from multiple sources
 *
 * Created by Pavel on 31.1.2017.
 */
interface DataProvider {

    /**
     * Returns Single returning all the currently available data
     */
    fun getData(forceReload: Boolean = false): Single<List<Movie>>

    /**
     * Returns Single returning next (currently uncached) page of data
     */
    fun getNextPage(): Single<List<Movie>>

    /**
     * Returns Single returning single movie by its id (server id)
     */
    fun getMovie(id: Long): Single<Movie>

}