package com.pavelskala.movies.model

import android.content.Context
import com.pavelskala.movies.MoviesConfig
import com.pavelskala.movies.db.MoviesDb
import io.reactivex.Single
import org.jetbrains.anko.defaultSharedPreferences
import java.util.*

/**
 * Created by Pavel on 31.1.2017.
 */
class DefaultDataProvider(
        private val ctx: Context,
        private val db: MoviesDb,
        private val moviesClient: MoviesClient)
    : DataProvider {

    override fun getData(forceReload: Boolean): Single<List<Movie>> {
        if (forceReload or isTimeForReload()) {
            return moviesClient.getPopularMovies()
                    .map { it.movies }
                    .doOnSuccess {
                        db.replacePopularMovies(it)
                        updatePreferences(1)
                    }
        } else {
            return Single.create { it.onSuccess(db.getPopularMovies()) }
        }
    }

    override fun getNextPage(): Single<List<Movie>> {
        val currentPage = ctx.defaultSharedPreferences.getInt(MoviesConfig.PREFERENCE_LAST_PAGE, 1)
        return moviesClient.getPopularMovies(currentPage + 1)
                .map { it.movies }
                .doOnSuccess {
                    db.addPopularMovies(it)
                    updatePreferences(currentPage + 1)
                }
    }

    override fun getMovie(id: Long): Single<Movie> {
        return Single.create { it.onSuccess(db.getPopularMovie(id)) }
    }

    private fun updatePreferences(pages: Int) {
        ctx.defaultSharedPreferences.edit()
                .putLong(MoviesConfig.PREFERENCE_LAST_UPDATED, Date().time)
                .putInt(MoviesConfig.PREFERENCE_LAST_PAGE, pages)
                .apply()
    }

    private fun isTimeForReload() =
            (ctx.defaultSharedPreferences.getLong(MoviesConfig.PREFERENCE_LAST_UPDATED, 0L) == 0L)

}