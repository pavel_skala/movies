package com.pavelskala.movies.util.extensions

import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.SelectQueryBuilder

/**
 * Extension allowing mapping columns from SELECT query result by lambda
 *
 * Created by Pavel on 31.1.2017.
 */
fun <T : Any> SelectQueryBuilder.parseList(
        parser: (Map<String, Any?>) -> T): List<T> =
        parseList(object : MapRowParser<T> {
            override fun parseRow(columns: Map<String, Any?>): T = parser(columns)
        })

/**
 * Extension allowing mapping columns from SELECT query result by lambda
 */
fun <T : Any> SelectQueryBuilder.parseSingle(
        parser: (Map<String, Any?>) -> T): T =
        parseSingle(object : MapRowParser<T> {
            override fun parseRow(columns: Map<String, Any?>): T = parser(columns)
        })
