package com.pavelskala.movies.util.delegates

import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by pskala on 2.2.2017.
 */
class NotNullSingleValueVarTest {

    var emptyProperty: String by DelegatesExt.notNullSingleValue()
    var twoTimesProperty: String by DelegatesExt.notNullSingleValue()
    var correctProperty: String by DelegatesExt.notNullSingleValue()

    @Test(expected = IllegalStateException::class)
    fun throwWhenNotInitialized() {
        val value = emptyProperty
    }

    @Test(expected = IllegalStateException::class)
    fun throwWhenAssignedTwoTimes() {
        twoTimesProperty = "1"
        twoTimesProperty = "2"
    }

    @Test
    fun correctAssignAndRead() {
        correctProperty = "1"
        assertEquals("1", correctProperty)
    }

}