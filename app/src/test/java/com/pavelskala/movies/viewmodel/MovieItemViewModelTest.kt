package com.pavelskala.movies.viewmodel

import android.content.Context
import com.nhaarman.mockito_kotlin.mock
import com.pavelskala.movies.model.Movie
import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by pskala on 2.2.2017.
 */
class MovieItemViewModelTest {

    companion object {
        const val TITLE = "Title"
        const val POPULARITY = 1.0
    }

    var context: Context = mock()

    @Test
    fun setData() {
        val viewModel = MovieItemViewModel(context)
        val movie = Movie(1, TITLE, "Overview", "Poster" , "Backdrop", POPULARITY, 20L, "2017-12-24")
        viewModel.setData(movie)
        assertEquals(TITLE, viewModel.name.get())
        assertEquals(POPULARITY, viewModel.score.get())
    }
}