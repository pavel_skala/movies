package com.pavelskala.movies.viewmodel

import com.nhaarman.mockito_kotlin.mock
import com.pavelskala.movies.model.DataProvider
import com.pavelskala.movies.model.Movie
import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by pskala on 2.2.2017.
 */
class MovieDetailActivityViewModelTest {

    companion object {
        const val TITLE = "Title"
        const val OVERVIEW = "Overview"
        const val POPULARITY = 1.0
        const val VOTES = 20L
        const val RELEASE = "2015-12-24"
    }

    var movieId = 1L
    var dataProvider: DataProvider = mock()

    @Test
    fun setData() {
        val viewModel = MovieDetailActivityViewModel(movieId, dataProvider)
        val movie = Movie(movieId, TITLE, OVERVIEW, "Poster" , "Backdrop", POPULARITY, VOTES, RELEASE)
        viewModel.setData(movie)
        assertEquals(TITLE, viewModel.name.get())
        assertEquals(OVERVIEW, viewModel.description.get())
        assertEquals(POPULARITY, viewModel.rating.get())
        assertEquals(VOTES, viewModel.votes.get())
        assertEquals(RELEASE.substring(0,4), viewModel.releaseYear.get())
    }

}